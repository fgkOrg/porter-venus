-- 任务所有权控制表 2019-03-08
DROP TABLE IF EXISTS `job_tasks_owner`;
CREATE TABLE `job_tasks_owner` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `job_id` bigint(20) DEFAULT '-1' COMMENT '任务id',
  `owner_level` int(2) DEFAULT '1' COMMENT '权限控制类型(1:人2:部门3:角色组)',
  `owner_id` bigint(20) DEFAULT '-1' COMMENT '所有者id',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `operator` bigint(20) DEFAULT '-1' COMMENT '操作人',
  `type` int(5) DEFAULT '1' COMMENT '类型(1：权限所有人 2：权限共享者)',
  `iscancel` int(2) DEFAULT '0' COMMENT '是否作废',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='任务所有权控制表';

-- 节点所有权控制表 2019-03-08
DROP TABLE IF EXISTS `b_nodes_owner`;
CREATE TABLE `b_nodes_owner` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `node_id` bigint(20) DEFAULT '-1' COMMENT '任务id',
  `owner_level` int(2) DEFAULT '1' COMMENT '权限控制类型(1:人2:部门3:角色组)',
  `owner_id` bigint(20) DEFAULT '-1' COMMENT '所有者id',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `operator` bigint(20) DEFAULT '-1' COMMENT '操作人',
  `type` int(5) DEFAULT '1' COMMENT '类型(1：权限所有人 2：权限共享者)',
  `iscancel` int(2) DEFAULT '0' COMMENT '是否作废',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1  DEFAULT CHARSET=utf8 COMMENT='节点所有权控制表';

-- 公共数据源配置表  2019-03-12
DROP TABLE IF EXISTS `b_public_data_source`;
CREATE TABLE `b_public_data_source` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `code` char(35) NOT NULL COMMENT '数据源编码',
  `name` varchar(50) DEFAULT NULL COMMENT '数据源名称',
  `xml_text` text COMMENT '数据源xml文本',
  `json_text` text COMMENT '数据源json文本',
  `declares` varchar(200) DEFAULT NULL COMMENT '数据源说明',
  `creator` bigint(20) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `state` int(5) NOT NULL DEFAULT '1' COMMENT '状态',
  `type` int(5) NOT NULL DEFAULT '1' COMMENT '类型',
  `ispush` int(5) DEFAULT '0' COMMENT '推送状态(0:新增 -1：回收 1：已推送)',
  `iscancel` int(2) DEFAULT '0' COMMENT '是否作废',
  `ispush` int(5) DEFAULT '0' COMMENT '推送状态(0:新增 -1：回收 1：已推送)',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE_CODE` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8 COMMENT='公共数据源配置表';

-- 权限控制2019-03-23
INSERT INTO `c_menu` VALUES (23, 'B001004', 'B001', '公共数据源', '/publicDataSource','fa-table', 2, 1, 1, 0, 1, 1, NULL);
INSERT INTO `c_role_menu` VALUES (30, 'A0002', 'B001004');

-- 公共权限控制表 2019-03-29
DROP TABLE IF EXISTS `c_data_authority`;
CREATE TABLE `c_data_authority` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `object_table` char(30) DEFAULT null COMMENT '目标表',
  `object_id` bigint(20) DEFAULT '-1' COMMENT '目标id',  
  `owner_level` int(2) DEFAULT '1' COMMENT '权限控制类型(1:人2:部门3:角色组)',
  `owner_id` bigint(20) DEFAULT '-1' COMMENT '所有者id',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `operator` bigint(20) DEFAULT '-1' COMMENT '操作人',
  `type` int(5) DEFAULT '1' COMMENT '类型(1：权限所有人 2：权限共享者)',
  `iscancel` int(2) DEFAULT '0' COMMENT '是否作废',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='数据权限控制表';

-- 任务权限操作类型表 2019-05-10
DROP TABLE IF EXISTS `d_control_type_plugin`;
CREATE TABLE `d_control_type_plugin` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `alert_type` varchar(100) DEFAULT NULL COMMENT '告警策略',
  `field_name` varchar(100) DEFAULT NULL COMMENT '页面字段名称',
  `field_code` varchar(100) DEFAULT NULL COMMENT '字段实际名',
  `field_order` int(10) DEFAULT NULL COMMENT '页面展示顺序',
  `field_type` varchar(100) DEFAULT NULL COMMENT '页面字段类型',
  `field_type_key` varchar(100) DEFAULT NULL COMMENT '页面字段类型对应字典',
  `state` int(10) DEFAULT NULL COMMENT '状态',
  `iscancel` int(10) DEFAULT NULL COMMENT '是否删除',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='任务权限操作类型表';

-- 权限控制操作类型表 2019-05-10
DROP TABLE IF EXISTS `r_owner_control`;
CREATE TABLE `r_owner_control` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `owner_type` int(10) DEFAULT NULL COMMENT '所有者权限类型(1:所有者 2.共享者 0:管理员).',
  `control_type_id` bigint(20) DEFAULT NULL COMMENT '操作类型id.',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='权限控制操作类型表';

-- 初始化任务权限操作类型  2019-05-10
INSERT INTO `d_control_type_plugin` VALUES (1, 'CHANGE', '移交', 'CHANGE', 1, 'RADIO', NULL, 1, 0, NULL);
INSERT INTO `d_control_type_plugin` VALUES (2, 'SHARE', '共享', 'SHARE', 2, 'RADIO', NULL, 1, 0, NULL);
INSERT INTO `d_control_type_plugin` VALUES (3, 'CANCEL', '作废', 'CANCEL', 3, 'RADIO', NULL, 1, 0, NULL);
INSERT INTO `d_control_type_plugin` VALUES (4, 'RECYCLE_C', '回收所有者', 'RECYCLE_C', 4, 'RADIO', NULL, 1, 0, NULL);
INSERT INTO `d_control_type_plugin` VALUES (5, 'RECYCLE_S', '回收共享者', 'RECYCLE_S', 5, 'RADIO', NULL, 1, 0, NULL);
INSERT INTO `d_control_type_plugin` VALUES (6, 'RECYCLE_A', '回收所有权限', 'RECYCLE_A', 6, 'RADIO', NULL, 1, 0, NULL);
-- 权限控制操作类型 2019-05-10
INSERT INTO `r_owner_control` VALUES (1, 1, 1);
INSERT INTO `r_owner_control` VALUES (2, 1, 2);
INSERT INTO `r_owner_control` VALUES (3, 1, 3);
INSERT INTO `r_owner_control` VALUES (4, 2, 3);
INSERT INTO `r_owner_control` VALUES (5, 0, 1);
INSERT INTO `r_owner_control` VALUES (6, 0, 2);
INSERT INTO `r_owner_control` VALUES (7, 0, 4);
INSERT INTO `r_owner_control` VALUES (8, 0, 5);
INSERT INTO `r_owner_control` VALUES (9, 0, 6);